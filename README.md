# 怒飞垂云-飞控硬件

#### 介绍
怒飞垂云-飞控及其外设硬件设计文件

PCB采用Altium Designer 20绘制，同时也提供了PDF版本原理图

#### 怒飞垂云”鹏心“通用型飞控，淘宝链接：
<a href="https://item.taobao.com/item.htm?spm=a230r.1.14.1.788a65aakfu1i6&id=628690434894&ns=1&abbucket=12#detail" target="_blank">https://item.taobao.com/item.htm?spm=a230r.1.14.1.788a65aakfu1i6&id=628690434894&ns=1&abbucket=12#detail</a>

#### 怒飞垂云官网：
<a href="http://www.nufeichuiyun.com/" target="_blank">http://www.nufeichuiyun.com/</a>

#### 《无人机固件开发教程》视频教程链接：
<a href="https://study.163.com/course/courseMain.htm?share=2&shareId=480000001921614&courseId=1209568864&_trace_c_p_k2_=501e97999e22444d916cb017ea970e48" target="_blank">https://study.163.com/course/courseMain.htm?share=2&shareId=480000001921614&courseId=1209568864&_trace_c_p_k2_=501e97999e22444d916cb017ea970e48</a>

### 怒而飞，其翼若垂天之云！